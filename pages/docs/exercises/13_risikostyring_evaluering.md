---
hide:
  - footer
---

# Øvelse 13 - Risikostyring: Evaluering af risici

## Information

![Risikostyringsprocessen4](../images/risikostyringsprocessen4.drawio.png){ width="100%" }  
_Risikostyringsprocessens 5 trin ifølge ISO/IEC 27005_  

Ud fra de identificerede risici og deres niveau (sandsynlighed og konsekvens) skal der nu foretages en en prioritering af risici, samt en evaluering om den enkelte risiko kan accepteres eller i stedet bør håndteres. Risici med både lav sandsynlighed og lav konsekvens er mindre vigtige at håndtere i forhold til risici med høj konsenkvens og/eller samndsynlighed.  

Det vil være ledelsen der fastsætter virksomhedens risikovillighed, i må her agere ledelse eller bruge jeres underviser hvis i ikke kan blive enige eller blot savner inspiration.

## Instruktioner

Brug information og eksempler på side 24-25 i _Guide til risikostyring (Dansk Standard DS/INF 10017:2023)_ når i arbejder. 
Det er vigtigt at i holder kompleksiteten lav. Derfor anbefales det at udføre følgende proces for kun et aktiv ad gangen. Hvis der er mange risici ved et aktiv kan i vælge at tage en risici ad gangen.  

1. Prioritér listen med risici i forhold til sandsynlighed og konsekvens med angivelse af niveau fra jeres risikomatrix.
2. Opdel listen i de risici der kan accepteres (grøn) og de risici der bør nedbringes (gul og rød).  
3. Opdel de risici der bør nedbringes i hhv. risici der kan accepteres af produktejer og de risici der skal accepteres på direktionsniveau. 

## Links

- Links til flere informationer som kan bruges til at studere øvelsens emne yderligere