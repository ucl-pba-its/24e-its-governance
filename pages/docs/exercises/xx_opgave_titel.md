---
hide:
  - footer
---

# Øvelse x - øvelse titel

## Information

- Baggrundsviden
- Teori
- Formål med øvelsen

## Instruktioner

- Hvad skal udføres (mere eller mindre instruerende)
- Hvad skal det bruges til (fremlæggelse, aflevering etc.)

## Links

- Links til flere informationer som kan bruges til at studere øvelsens emne yderligere