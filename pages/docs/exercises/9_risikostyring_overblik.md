---
hide:
  - footer
---

# Øvelse 9 - Risikostyring: Overblik

## Information

Alle virksomheder beskæftiger sig med risici. Det kan være forretningsmæssige risici, hvad skal vi satse på, hvilket marked vil vi sælge vores produkter på osv.  
I forhold til sikkerhed bør virksomheder også gøre op med sig selv hvilke risici de er parate til at tage med hensyn til informationssikkerhed og cybersikkerhed.

***"Informationssikkerhed handler om beskyttelse af informationer - og her er der tale om alle informationer, hvad enten de er digitale (på en computer, i en sky, på en server osv.) eller fysiske (fx i et dokument i et arkivskab).***

_**Cybersikkerhed handler bl.a. også om at beskytte informationer, men er kun fokuseret på digitale informationer.**_  

**_Cybersikkerhed handler desuden ikke udelukkende om beskyttelse af informationer, men også om forsvar af enheder eller produkter, der er koblet på internettet"_**  
_Guide til risikostyring (Dansk Standard DS/INF 10017:2023, s. 9)_

I de følgende øvelser skal i arbejde med den risikostyringsproces der anvendes i  
**ISO/IEC 27005:2022 - Informationssikkerhed, cybersikkerhed og privatlivsbeskyttelse – Vejledning i styring af informationssikkerhedsrisici**  
Processen er fremstillet grafisk på side 8 i ISO/IEC 27005:2022.  

En forsimplet version af processen er vist her. Modellen er en kopi af modellen fra dokumentet  
_Guide til risikostyring (Dansk Standard DS/INF 10017:2023)_ som i har læst til i dag.  

![Risikostyringsprocessen0](../images/risikostyringsprocessen0.drawio.png){ width="100%" }  
_Risikostyringsprocessens 5 trin ifølge ISO/IEC 27005_

Processen er delt i 5 trin:  

**Trin 1:** Etablering af kontekst  
**Trin 2:** Identifikation af risici  
**Trin 3:** Analyse af risici  
**Trin 4:** Evaluering af risici  
**Trin 5:** Håndtering af risici    

Læg mærke til at der er en pil fra trin 5 til trin 1. Det er der fordi at processen er iterativ. 
Det betyder at den gentages så længe virksomheden har risici der skal styres, hvilket i praksis betyder i hele virksomhedens levetid.  

Læg også mærke til at der er en pil fra trin 5 og frem. Den angiver om risikoen er nedbragt til et acceptabelt niveau, altså om risikoen kan accepteres.  

Igennem øvelserne (9 - 14) er det en fordel at benytte Guide til risikostyring (Dansk Standard DS/INF 10017:2023) - som kan findes på itslearning.  
I bør også have ISO/IEC 27005:2022 klar hvis det bliver nødvendigt at bruge den.  

Endelig kan i få behov for at anvende viden, standarder og love som i allerede har arbejdet med i øvelserne i de foregående uger.  

Når i arbejder er det vigtigt at i spørger jeres underviser hvis der er spørgsmål i ikke kan besvare eller i mangler inspiration til at udføre dele af øvelserne.

## Instruktioner

1. Lav et fælles dokument i deler i gruppen.  
2. Beskriv et virksomhedseksempel. Det må meget gerne være en virksomhed i kender i forvejen, hvis det ikke er muligt er det fint at konstruere en virkomhed. Uanset hvad i vælger skal i tage nedenstående parametre med når i beskriver virksomheden.  
En mere detaljeret beskrivelse af parametrene kan i læse i kapitel 2 i  
_Guide til risikostyring (Dansk Standard DS/INF 10017:2023)_  
Figur 1 (side 11) i vejledningen giver 3 eksempler på virksomhedseksempler.    
    - digitalisering af virksomheden
    - fortrolighed af data anvendt i virksomheden
    - virksomhedens placering i leverandørhierarkiet
    - antallet af brugere.

## Links

- Guide til risikostyring (Dansk Standard DS/INF 10017:2023) - kan findes på itslearning
- ISO/IEC 27005:2022 - kan findes på dansk standard distribute - se [øvelse 6](../exercises/6_standards.md)
- Ressourcer fra øvelserne i har arbejdet med i tidligere uger (lovgivning, standarder, viden om virksomheder, trusselsbillede mm.) 