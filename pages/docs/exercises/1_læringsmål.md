---
hide:
  - footer
---

# Øvelse 1 - Fagets læringsmål

## Information

I denne øvelse skal i undersøge fagets læringsmål.

Formålet er at sikre at i ved:

- hvor læringsmålene kan findes
- hvad de betyder
- hvad i kan bruge dem til

og ikke mindst, bliver det lettere at huske læringsmålene hvis i har arbejdet lidt med dem.

## Instruktioner

1. Find læringsmålene og del linket mellem jer
2. I jeres gruppe tal sammen om, hvordan de er relevante for faget og hvordan i forstår/ikke forstår dem.Skriv de spørgsmål ned i identificerer mens i taler.
3. Forbered at præsentere et læringsmål på klassen (ikke på tavlen, bare siddende)

## Links

- [https://www.ucl.dk/studiedokumenter/it-sikkerhed](https://www.ucl.dk/studiedokumenter/it-sikkerhed)
- [https://www.mitucl.dk/uddannelser/it-sikkerhed](https://www.mitucl.dk/uddannelser/it-sikkerhed)
