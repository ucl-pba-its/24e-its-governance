---
hide:
  - footer
---

# Øvelse 11 - Risikostyring: Identifikation af risici 

## Information

![Risikostyringsprocessen2](../images/risikostyringsprocessen2.drawio.png){ width="100%" }  
_Risikostyringsprocessens 5 trin ifølge ISO/IEC 27005_

Identifikation af risici kan inddeles i to processer: 

### 1. Identifikation og beskrivelse af informationssikkerhedsrisici

Denne proces bruges til at finde potentielle risici. Det kan gøres ved to forskellige tilgange, hændelsesbaseret og aktivbaseret tilgang.  
Den hændelsesbaserede tilgang tager udgangspunkt i hændelser der kan ramme virkosmheden og har primært forkus på konsekvenserne.  
Aktivbaseret tager udgangspunkt i virksomhedens væsentlige aktiver og har primært forkus på sandsynligheden.  
Fælles for begge tilgange er at i får udarbejdet en liste over identificerede risici. Listen skal beskrive de enkelte risici tilstrækkeligt til at i efterfølgende kan foretage risikoanalysen.  

Til at identificere risici kan i bruge ressourcer på internettet. For eksempel CVE databasen fra Mitre, Trusselskatalog på sikkerdigital eller listen over trusler og sårbarheder i ISO/IEC 27005:2022 (Anneks A - tabel A.7 - A.9).

### 2. Identifikation af risikoejere  

Det er vigtigt at udpege risikoejere for hver risici. Hvis en risiko ikke har nogen ejer er der nok ikke nogen der føler ansvar for at håndtere et problem hvis det opstår.  
Det vil typisk være ledere der udpeges som risikoejere. Jo højere konsekvens for virksomheden jo længere oppe i hierakiet skal risikoejerne findes.  

## Instruktioner

1. Brug information og eksempler på side 19-21 i _Guide til risikostyring (Dansk Standard DS/INF 10017:2023)_ når i arbejder.
2. Beslut om i vil anvende en hændelsesbaseret eller aktivbaseret tilgang til at identificere risici.
3. Identificer mindst 3 risici i jeres virksomhed (meget gerne flere).
4. Beskriv hvem der er ansvarlig for de enkelte risici, brug de ledelsestitler i arbejdede med i [øvelse 2](../exercises/2_business_organisation.md)
5. Lav en liste over identificerede risici med ejere tilknyttet.


## Links

- ...