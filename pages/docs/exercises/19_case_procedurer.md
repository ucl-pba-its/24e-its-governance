---
hide:
  - footer
---

# Øvelse 19 - Virksomhedscase: Business continuity

Dette er en gruppeøvelse.

## Information

Formålet med denne øvelse er at opnå viden om hvad business continuity er og hvordan det relaterer til sikkerhed.
Øvelsen tager udgangspunkt i sikker digitals materiale om beredskabsstyring, her er både litteratur, skabeloner og videomateriale om emnet.

Business continuity er del af foranstaltning 5.30 i ISO/IEC 27002:2022 - _IKT Parathed til understøttelse af business continuity_
Formålet med foranstaltningen er at sikre, at organisationens information og understøttende aktiver er tilgængelige under driftsforstyrrelser. 

Et dansk ord for business continuity er IKT kontinuitetsplaner som omfatter beredskabs- og reetableringsprocedurer.  

Nedenstående illustration angiver hvad der kan indgå i en business continuity plan samt hvilke dele der omhandler information.  

![Galotti, Cesare. _Information Security_ (s. 226)](../images/business_continuity_plan.drawio.png)  
Galotti, Cesare. _Information Security_ (s. 226)

Formålene med beredskabsstyring er grundlæggende at:    

- Undgå driftstop
- Minimere nedetid
- Minimere langtidskonsekvenser
- Sikre at planer bliver sat i værk hvis det er nødvendigt

Beredskab i forhold til informationssikkerhedsarbejdet skal sikre at:    

- systemer der bruger informationer er tilgængelige
- at de data organisationen har er de rigtige
- indæmme informationslæk hurtigst muligt

Beredskabet aktiveres når det opdages at processer ikke kan undføres som normalt. Der kan ikke logges ind i systemer, kommunikation fungerer ikke som normalt osv.  
En del af de ting som kan aktiverer beredskabet er identificeret i risikoanalysen som hændelser og aktiver.

Der findes andre beredskaber (brand, politi osv.) som i samspil med informationssikkerhedsberedskabet kan understøtte at informationssikkerheden opretholdes under en hændelse.  

Etablering og vedligeholdelse af informationssikkerhedsberedskab følger de fire faser:  

- PLAN 
- DO 
- CHECK 
- ACT

Herunder forklares indholdet af de fire faser.

### Plan (Lav en plan - analyse og forberedelse)

Denne fase er planlægning: rammen sættes, hvad kan vi tåle af nedetider, hvor længe kan vi drifte uden forskellige services, hvor mange data kan vi undvære, hvor hurtigt skl vi være i drift igen.

Det er relevant at udføre følgende i denne fase:  

1. Overvej hvad kan der ske hos organisationen, hvad er det værste der kan ske (tag udgangspunkt i risikovurderinger) - find ud af hvem der er ansvarlig for hvad. 
2. Hvad er det værste der kan ske, hvilke scenarier kan vi forestilles os og hvem skal involveres i de forskellige scenarier. 
3. Etablere acceptkriterier - hvor lang tid, reetablering osv.
4. Identificere de scenarier der ikke kan håndteres på f.eks papir eller andre måder - det er dem der skal medtages i beredskabet.

#### BIA

I denne fase laves også en konsekvensanalayse (business impact analysis - BIA).

Formålet med en konsekvensanalyse er at fastsætte den maksimalt acceptable nedetid for en organisations aktiviteter under en hændelse (Maximum Tolerable Period of Disruption - MTPD) samt at identificere hvilke ressourcer nedetiden gælder for.  
Altså hvilke systemer og informationer der er utilgængelige mens hændelsen foregår.  
En BIA fastsætter også en organisations minimumsperformance under en hændelse (Level of Business Continuity - LBC). 
Endelig etableres det maksimale datatab der kan tolereres (maximum tolerable data loss MTDL)

#### Reetableringsprocedurer (disaster recovery)

Reetableringsprocedurer er uarbejdelse af mål og strategier for reetablering af information og systemer (recovery objectives and strategies).
Reetableringsprocedurer etablerer målbare metrikker som beredskabet kan arbejde efter ved reetablering.  
Relevante metrikker er:  

- Recovery Time Objective (RTO) - Maksimum tide det må tage at reetablere tilgængelighed til information
- Recovery Point Objective (RPO) - Tidspunkt hvor data er konsistent og kan reetableres. For eksempel intervallet mellem backups.
- Minimum Business Continuity Objective (MBCO) - De ressourcer der som minimum skal være til stede under en hændelse. 

Reetableringsprocedurer bør udføres for hver enkelt it-system, service og ikke-digitale aktiver.

### Do (implementere planen)

Planen er nu lavet og skal implementeres

1. Hvem er ressourcerne, roller i organisationen, hvem har de forskellige roller i vores beredskab. Sikre at ressourcerne er der i organisationen eller udenfor.
2. Hvor skal vi være hvis beredskabet aktiveres - lokaler, kommunakation, hjælp i organisationen når alt det andet ikke virker.
3. Teste at det virker - prøve det af.

### Check (teste planen)

De findes fire modeller til at teste om planen virker.

1. Skrivebordsøvelse - stemmer papirene og har vi husket det hele ?
2. Skrivebordsøvelse - kan processen gennemføres ?
3. Scenariebaserede øvelser - et scenarie vælges og der trænes i om det er muligt at håndtere scenariet - kan være både skrivebord og tekniske øvelser
4. Den ultimative - der slukkes for noget, noget gøres utilgængeligt så det ikke fungerer

**Udførelse af skrivebordsøvelse:** 
Hvor beredskabsplanen og medlemmer af beredskabet får lov til at øve deres roller (leder, skrive log, kommunikere eksternt osv.)  
Øvelsen er en kvalitetssikring af at alle materialer er til rådighed og alle forstår rollerne.  
Sikker digital har et beredskabsspil som i skal spille sammen :-)

### Act (løbende forbedre og kvalitetssikre planen)

Her evalueres om det der er planlagt virker, holder planerne/processerne og er kriseberedskab og deres roller godt fungerende?  
Nye ansatte i kriseberedskabet, nye risikovurderinger mm. kan være anledning til at Act skal udføres. 

Under Act evalueres:  

1. Beredskabbeskrivelser og processer
2. Erfaring fra test og måske virkelige hændelser?

### Opsamling efter en hændelse

Efter en hændelse mødes kriseberedskabet igen til debriefing - målet er at gennemgå oplevelsen og lære af de nye erfaringer.  
Det involverer at kigge på beredskabsloggen og dem der har lavet beredskabsplanen skal lære af hvordan dem der gennemførte beredskabsplanen arbejdede.

Erfaringer bør bruges til at kontrollere risikovurderingen, med henblik på at justere eller bekræfte antagelserne i risikovurderingen.    

Eksempler på relevante spørgsmål i debriefingen: 

- Gjorde vi det rigtige?   
- Var der tidlige tegn vi ikke anvendte?  
- Var der procedurer som ikke var tilstrækkelige?  
- Var de kontaktlister der ikke var opdaterede?  

## Instruktioner

**Del 1 - informationer og BIA** 

1. I fællesskabs ser vi filmen og besvarer spørgsmålene på [Beredskabsstyring - Sikker digital](https://www.sikkerdigital.dk/myndighed/iso-27001-implementering/beredskabsstyring)
2. Læs om planlægning af beredskabsstyring [Planlægning af beredskabsstyring - Sikker Digital](https://www.sikkerdigital.dk/myndighed/iso-27001-implementering/beredskabsstyring/planlaegning)
3. Prøv at lave en lille BIA på _**Sentinel Surveillance Systems**_:
    - Tal sammen i gruppen om hvorfor manglende it-understøttelse vil være mere eller mindre kritisk på de forskellige områder hos _**Sentinel Surveillance Systems**_
    - Identificer så mange kritiske forretningsprocesser som muligt.
    - I burde nu have et overblik over hvilke processer der vil kunne udføres uden it-understøttelse og dermed kunne opretholdes under en it-sikkerhedshændelse

**Del 2 - Beredskabsspillet fra sikker digital**

4. Læs om [beredskabsspillet](https://www.sikkerdigital.dk/myndighed/iso-27001-implementering/beredskabsstyring/beredskabsspillet)
5. Udfyld en miniberedskabsplan for _**Sentinel Surveillance Systems**_ - gerne baseret på jeres BIA - link til [miniberedskabsplan](https://www.sikkerdigital.dk/media/6804/miniberedskabsplan-2020-3.pptx)
6. Læs i fællesskab [vejledning til facilitator](https://www.sikkerdigital.dk/media/7007/vejledning-til-facilitator-2020-4.pptx)
7. Hent en case fra [beredskabsspillet](https://www.sikkerdigital.dk/myndighed/iso-27001-implementering/beredskabsstyring/beredskabsspillet)
8. Fordel roller - i skal være 7 eller flere til spillets roller - en af jer skal være facilitator
9. Facilitator starter spillet og de 3 faser gennemføres
10. Gennemfør evaluering - husk at facilitator har en stor rolle her
11. Gentag del 2 med en ny case hvis i har mere tid 


## Links

- Galotti, Cesare. _Information Security_ kapitel 12.14 kan anvendes som supplerende materiale.