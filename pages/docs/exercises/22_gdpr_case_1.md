---
hide:
  - footer
---

# Øvelse 22 - GDPR i virkeligheden 1: Danske kommuner

## Information

Virkeligheden er ofte meget kompleks i forhold til den undervisning i deltager i her på skolen.  
Det er godt at slippe for kompleksitet når nyt skal læres, men omvendt kan det godt føre til en lidt forenklet opfattelse af, hvordan teori skal omsættes til praksis.  

I danske virksomheder og kommuner har der været it-systemer og behandling af personoplysninger, i meget længere tid end for eksempel GDPR lovgivningen har eksisteret. 
Samtidig er virksomheder, kommuner, organisationer osv. underlagt en begrænset mængde ressourcer, det er desværre ikke usædvanligt at høre om nedskæringer og begrænsede budgetter.  
Årsagerne til begrænsede ressourcer kan være mange og det kan være svært at skulle prioritere, når der ikke er ressourcer til at gøre alt.  

Men loven er vel stadig gældende og den skal vel overholdes??  
Desværre er det ikke alle steder den bliver det.....  

I sidste uge udkom Version 2 med en artikel om datatilsynets skriftlige tilsyn med 48 kommuner, den er et tydeligt bevis på at virkelighedens praksis langt fra er ideel.  

Formålet med øvelsen er at belyse et GDPR relateret dilemma fra virkeligheden og samtidig prøve at genfinde de artikler i GDPR som er aktuelt i forhold til dilemmaet.  

**Vi samler op på klassen efter i har arbejdet med øvelsen.**

## Instruktioner

1. Læs artiklen som er i dagens plan på itslearning.  
Filen hedder *"Ny kontrol afslører_ Kommuner sylter basale it-sikkerhedstiltag _ Version2.pdf"*  
2. Tal sammen i jeres gruppe om hvad artiklen indeholder. Synes i det er ok at kritisere kommunerne eller mener i at kommunerne gør hvad de kan?
3. Hvilket dilemma er det it-chefen fremlægger og hvordan ville i forholde jer til dilemmaet hvis i arbejdede i en kommune?
4. Hvor er det der især er udfordringer i danske kommuner?
5. Hvilke dele af GDPR mener i bliver misligeholdt af de nævnte udfordringer (med henvisning til kapitel og artikel i GDPR)

**BONUS HVIS TID**  

1. Hvad er de tekniske minimumskrav der henvises til i datatilsynets pressemeddelelse (link herunder) 

## Links

- [Datatilsynet pressemeddelelse 10/24: behandlingssikkerheden i 48 kommuner](https://www.datatilsynet.dk/presse-og-nyheder/nyhedsarkiv/2024/okt/datatilsynet-har-afsluttet-skriftlige-tilsyn-med-behandlingssikkerheden-i-48-kommuner)
- [GDPR.DK](https://gdpr.dk)
- [Europa-Parlamentets og Rådets forordning (EU) 2016/679 af 27. april 2016](https://eur-lex.europa.eu/legal-content/DA/TXT/PDF/?uri=CELEX:32016R0679)
- [Databeskyttelsesloven - LBK nr 289 af 08/03/2024](https://www.retsinformation.dk/eli/lta/2024/289)