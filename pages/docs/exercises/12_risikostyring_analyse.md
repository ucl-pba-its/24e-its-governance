---
hide:
  - footer
---

# Øvelse 12 - Risikostyring: Analyse af risici

## Information

![Risikostyringsprocessen3](../images/risikostyringsprocessen3.drawio.png){ width="100%" }  
_Risikostyringsprocessens 5 trin ifølge ISO/IEC 27005_

Risikoanalysen fastsætter en værdi for de enkelte risici ved hjælp af en beregning af sandsynlighed og konsekvens.  
Formlen for risiko er sandsynlighed gange konsekvens. 

I arbejdet med konsekvens arbejdes med [CIA modellen](https://www.fortinet.com/resources/cyberglossary/cia-triad)  

**C** = Confidentiality (fortrolighed)  
**I** = Integrity (integritet)  
**A** = Availability (tilgængelighed)  

Måden modellen anvendes på i analysearbejdet er til at fastslå konsekvensen ved tab af enten fortrolighed, tilgængelighed eller integritet for de enkelte aktiver.  

Analysen er ofte en proces der udføres af personer der kender aktiverne og dermed kan være med til at fastslå konsekvensen. De har også ofte indsigt i sandsynligheden for at det kan ske ud fra historisk viden. Her bør selvfølgelig også tages hensyn til det generelle trusselsbillede.  

Når i arbejder skal i huske at bruge de skalaer for sandsynlighed og konsekvens i udarbejdede i etablering af kontekst.  

## Instruktioner

1. Brug information og eksempler på side 22-23 i _Guide til risikostyring (Dansk Standard DS/INF 10017:2023)_ når i arbejder.  
2. Tal i gruppen om hver af jeres identificerede risici og fastslå konsekvens samt sandsynlighedsniveau for hver risici.
3. Beskriv hver risici på samme måde som eksemplerne i guiden (side 23)

## Links

- CIA model [https://medium.com/learn-and-grow/what-is-the-cia-model-ae160bac21f](https://medium.com/learn-and-grow/what-is-the-cia-model-ae160bac21f)
- CIA & DAD model [https://medium.com/@yadavtejas249/cia-and-dad-triad-ef84a94f9aee](https://medium.com/@yadavtejas249/cia-and-dad-triad-ef84a94f9aee)