---
hide:
  - footer
---

# Øvelse 5 - Trusselsbillede, et overblik

## Information


Formålet med øvelsen er at give en kontekst til arbejdet med sikkerhed i it-governance. Det er essentielt for arbejdet at have viden om hvad der truer virksomhedernes systemer og informationer.  
Det er også et nødvendigt afsæt til at arbejde med både lovgivning og standarder. Det vil hjælpe med at forstå nødvendigheden af det (ofte store) arbejde der skal lægges i at sikre virksomheden. Det vil også give en forståelse af forskellig motivation som trusselsaktører har og at det ikke er alle trusler der er aktuelle for alle virksomheder og organisationer.

Det samlede overblik kaldes for "trusslesbilledet" - i denne kontekst er det alle trusler som er aktuelle.  
I en virksomheds/organisationskontekst vil det være en eller flere kategorier af trusler der vil være aktuelle.  

Trusselsbilledet refererer til de forskellige typer trusler, angreb og sårbarheder, som digitale systemer, netværk og data står over for.  
Det omfatter et bredt spektrum af cybertrusler, der konstant udvikler sig i takt med teknologiske fremskridt og ændringer i angribernes metoder.    

En ganske udemærket bog [Cybertrusler - det digitale samfunds skyggeside](https://djoefforlag.dk/products/cybertrusler) introducerer læseren til de mest centrale trusler fra cyberspace samt til de mange forsøg på at håndtere disse. Efter en indledning og en historisk indplacering af problemfeltet, kortlægger og beskriver bogen de syv primære cybertrusler: 

- cyberkrig
- desinformation
- cyberterrorisme
- cyberspionage
- overvågning
- cyberkriminalitet
- hacktivisme  

Når i arbejder så husk at bruge de links jeg har givet i bunden af øvelsen.

## Instruktioner

1. Rangér kategorierne (de syv primære cybertrusler) efter cfcs trusselsniveau for hver kategori hvor det er muligt - brug højest 10 minutter på dette
2. Find eksempler fra virkeligheden (husk et link som reference, eksempler er herunder) der passer på hver kategori, start med dem der har det højeste CFCS trusselsniveau.
    - Cyberkrig (Ukraine/Rusland?)
    - Politisk påvirkning (Valg i USA?)
    - Cyberterrorisme (elbiler? kritisk infrastruktur?)
    - Cyberspionage (mod lande eller virksomheder?)
    - Cyberkriminalitet (Er der penge i det?)
    - Hacktivisme (er hackere onde??)
    - Overvågning (hvem kan dog finde på at overvåge andre?)
3. Vi taler i fællesskab på klassen om hvad i har fundet frem til. 

## Links

Trusselsniveau:  
- [CFCS - Cybertruslen mod Danmark](https://www.cfcs.dk/da/cybertruslen/)

Eksempler fra virkeligheden:  
- [Anonymous](https://en.wikipedia.org/wiki/Anonymous_(hacker_group))  
- [Cult of the dead cow](https://en.wikipedia.org/wiki/Cult_of_the_Dead_Cow#Hacktivism)   
- [Putins propagandamaskine: Sådan ser krigen ud på russisk stats-tv](https://www.dr.dk/nyheder/udland/hvilke-loegnhistorier-fortaeller-russiske-medier)  
- [The U.S.-China Cyber Espionage Deal One Year Later](https://www.cfr.org/blog/us-china-cyber-espionage-deal-one-year-later)  