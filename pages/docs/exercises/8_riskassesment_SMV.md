---
hide:
  - footer
---

# Øvelse 8 - Risikostyring del 1 (Forberedelse til undervisningsgang 3)

Noter:  

Del 1: Hvad er risikostyring?

Risikostyringsprocessen. 

Del 2: Etablering af kontekst

Jeg laver en virksomhedsbeskrivelse.  
Brug skabelon fra DS.
Skal der være mere end en virksomhed?

Del 3: Identifikation af risici

Hændelsesbaseret - hvad kan ramme vores virksomhed  
Aktivbaseret - 

Del 4: Analyse af risici

Del 5: Håndtering af risici


## Information

- Baggrundsviden
- Teori
- Formål med øvelsen

## Instruktioner

- Hvad skal udføres (mere eller mindre instruerende)
- Hvad skal det bruges til (fremlæggelse, aflevering etc.)

## Links

- Links til flere informationer som kan bruges til at studere øvelsens emne yderligere