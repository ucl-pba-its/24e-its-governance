---
hide:
  - footer
---

# Øvelse 7 - Lovgivning i sikkerhedsarbejdet

## Information

I denne øvelse får du kendskab til lovgivning inden for sikkerhed.  
Formålet med øvelsen er at få et overblik over hvilken lovgivning der er relevant i arbejdet med sikkerhed.   

### Hvorfor er det nødvendigt med lovgivning?

Virksomheder udfører aktiviteter der gavner forretningen, altså aktiviteter der tilfører virksomheden værdi.  
At vedligeholde sikkerheden i et stykke software i hele produktets levetid kan være en stor omkostning og derfor vil virksomheder typisk fravælge at vedligeholde sikkerheden når produktet ikke sælger særlig meget mere.  

Samtidig kan det være en stor omkostning at arbejde organisatorisk med sikkerhed hvis virksomheden ikke har prøvet det før. Det kræver uddannelse, kortlægning af processer og muligvis ansættelse af nyt personale.  

Ovenstående argumenter er forståelige når de sættes i en cost/benefit kontekst men i en sikkerhedskontekst kan det virke uansvarligt.  

Et andet problem angående cybersikkerhed omhandler virksomheder i lande uden for EU, her ses eksempler på produkter der i bedste tilfælde ikke overholder sikkerhedskrav og i værste tilfælde lækker f.eks. personoplysninger uden brugerens viden.  
Ofte er disse produkter også så billige at virksomheden bag ikke bruger ressourcer på at holde sikkerheden i produkterne opdateret.  

Uanset argumentet og konteksten vil svag eller manglende sikkerhed være til fordel for trusselsaktører.  
Det giver dem mulighed for at bryde ind, i første omgang i produktet, men ofte også til bagvedliggende systemer.  

**Kort sagt** Virksomheder gør det som de anser som god forretning.  
Med lovgivning kan sikkerhed blive en god forretning da mangel på sikkerhed kan føre til både bøder og skader på virksomhedens omdømme.  
Faktisk kan god sikkerhed bruges som et salgsparameter som øger virksomhedens troværdighed i forhold til konkurrenterne.  

## Forordninger og direktiver

Lovgivningen som vi arbejder med i sikkerhed gælder i hele EU. Dog er der forskel på hvordan det implementeres, her er det afgørende om der er tale om en forordning eller et direktiv.  

Forordninger er bindende retsakter, der har direkte anvendelse i alle EU-medlemslande, så snart de træder i kraft.  
Det betyder, at forordninger gælder som lov i hvert land uden behov for yderligere national lovgivning.  
Medlemslandene skal altså ikke tilpasse deres lovgivning for at implementere en forordning.  
Et eksempel er GDPR (Databeskyttelsesforordningen), der trådte direkte i kraft i alle EU-lande.  

Direktiver er også bindende retsakter, men de fastsætter mål, som hvert medlemsland skal opnå.  Dog er det op til de enkelte lande selv at beslutte, hvordan de vil gennemføre direktivet i deres nationale lovgivning.  
Medlemslandene får en tidsramme til at tilpasse deres love for at opfylde direktivets krav.  
Et eksempel er NIS2-direktivet om cybersikkerhed, som medlemslandene skal implementere gennem national lovgivning.  

**Kort sagt:** forordninger er direkte gældende som lov i hele EU, mens direktiver skal implementeres gennem national lovgivning, hvor landene har frihed til at bestemme formen og metoden.

Det er forskelligt hvilke områder de enkelte forordninger og direktiver dækker, herunder gives 3 eksempler på problemer og hvordan de imødekommes af lovgivning.  

Det drejer sig om hhv. 
- Software/systemer
- Kritisk infrastruktur
- Håndtering af persondata.

## Problem 1: Sårbarheder i software/systemer

Software er alle steder, det er sjældent at systemer og hardware er uden software og de fleste systemer er også tilkoblet internettet.  
Det er også softwaren der er målet for mange trusselsaktører, der er utallige eksempler fra medierne om systemer der er blevet kompromitteret og sat ud af drift, midlertidigt eller permanent på grund af usikkerheder i software.  

Et godt eksempel er fra november 2022 hvor DSB toge i hele landet holdt stille i et lille døgn, fordi en underleverandørs testmiljø var blevet kompromitteret.  
[DSB: Lørdagens totalnedbrud af togdriften skyldtes hackerangreb og fejl i nødprocedure](https://www.dr.dk/nyheder/seneste/dsb-loerdagens-totalnedbrud-af-togdriften-skyldtes-hackerangreb-og-fejl-i)  

Kendte sårbarheder i systemer registreres i CVE-systemet (Common Vulnerabilities and Exposures).  
CVE-systemet er en globalt anerkendt standard, der bruges til at identificere og beskrive sikkerhedssårbarheder i software, hardware og systemer.  
Formålet med CVE-systemet er at skabe en fælles måde at referere til og dele oplysninger om sårbarheder på, hvilket gør det lettere for både virksomheder, it-specialister og sikkerhedseksperter at forstå og håndtere trusler.  

CVE systemet er et godt sted at undersøge sårbarheder hvis du ønsker at dykke dybere i hvilke sårbarheder der findes.  

Hovedkomponenterne i CVE-systemet er:  

**CVE-ID**  
Hver sårbarhed i CVE-databasen tildeles en unik identifikator, kaldet en CVE-ID. Denne består af præfikset "CVE", et årstal, og et unikt nummer, f.eks. [CVE-2024-7965](https://www.cve.org/CVERecord?id=CVE-2024-7965).  
Dette gør det nemt at referere til en specifik sårbarhed på tværs af forskellige sikkerhedsværktøjer og rapporter.  

**Beskrivelse af sårbarhed**  
Hver CVE-post indeholder en kort beskrivelse af sårbarheden, herunder hvad den påvirker og hvilke typer angreb eller udnyttelser den muliggør.  

**CVSS (Common Vulnerability Scoring System)**  
CVE-poster kan være tilknyttet en CVSS-score, som kvantificerer sårbarhedens alvorlighed på en skala fra 0 til 10, hvor 10 er den mest kritiske.   

**CVE-database**  
CVE-databasen vedligeholdes af organisationen [MITRE](https://www.mitre.org/). Databasen er frit tilgængelig og kan ses her: [https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=CVE-2024-7965](https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=CVE-2024-7965)  

I skrivende stund er der **240830** sårbarheder beskrevet i databasen og der kommer hver dag nye til.  
De fleste af dem er heldigvis ikke til fare mere da de er blevet udbedret, men omfanget giver et billede af hvor stort et problem sikkerhed i software og systemer er. 

### Lovgivning 1: Cyber Resiliency Act (CRA)  

CRA er en EU forordning, altså direkte lovgivning fra EU.  

Forordningen er et led i den europæiske strategi for cybersikkerhed, og ambitionen er at styrke cybersikkerheden i produkter med digitale elementer og dermed øge tilliden og samtidig sikre retssikkerheden.  

Med forordningen følger fælleseuropæiske cybersikkerhedskrav for alle, der fremstiller og udvikler produkter med digitale elementer, herunder både software og hardware.  

Min tolkning af årsagen til forordningen er at den er nødvendig for at give virksomheder et incitament til at implementere og vedligeholde sikkerheden i deres digitale produkter.  
Lidt firkantet kan man sige at de får vredet armen om på ryggen og nu skal forholde sig til sikkerhed!   

Du kan læse mere om CRA her:  
_[Dansk Standard om CRA](https://www.ds.dk/da/i-fokus/lovgivning/lovgivning-paa-det-digitale-omraade-og-standarder/cyber-resilience-act)_  

Vi arbejder ikke yderligere med CRA i dette fag men nu har du viden om lovgivningen og hvorfor den er nødvendig.

## Problem 2: Sårbarheder i kritisk infrastruktur  

Kritisk infrastruktur er den infrastruktur der er vital for opretholdelse af samfund og menneskeliv.  

Det er sektorer af særlig kritisk betydning: energi, transport, bankvirksomheder, finansielle markedsinfrastrukturer, sundhed, drikkevand, spildevand, digital infrastruktur, forvaltning af IKT-tjenester, offentlig forvaltning og rummet.  
Det er også andre kritiske sektorer: post- og kurertjenester, affaldshåndtering, fremstilling, produktion og distribution af kemikalier, produktion, tilvirkning og distribution af fødevarer, fremstilling, digitale udbydere og forskning.  

Fordi disse sektorer er kritiske for samfundet er det vigtigt at de er rigtig godt beskyttet af både tekniske og organistoriske foranstaltninger.  

Desværre er det ikke helt sådan endnu, rigsrevisionen har udtalt kritik af statens IT hvor [syv af 12 samfundskritiske it-systemer dumper](https://nyheder.tv2.dk/politik/2023-12-04-statens-it-faar-skarp-kritik-for-daarlig-sikkerhed-syv-af-12-samfundskritiske-it-systemer-dumper).  

Energinet, leverandør af el til hele danmark får også kritik fra rigsrevisionen  
[Rigsrevisionen kritiserer Energinet for forsinket færdiggørelse af tiltag til forbedring af IT-sikkerhed](https://energinet.dk/om-nyheder/nyheder/2024/05/28/rigsrevisionen-kritiserer-energinet-for-forsinkelse-i-handlingsplan-for-forbedring-af-it-sikkerhed/)  

SektorCERT, de kritiske sektorers cybersikkerhedscenter udgav i 2023 en rapport om det største cyberangreb mod dansk, kritisk infrastruktur som [SektorCERT](https://sektorcert.dk) kender til:  
[Rapport: Angrebet mod dansk, kritisk infrastruktur](https://sektorcert.dk/wp-content/uploads/2023/11/SektorCERT-Angrebet-mod-dansk-kritisk-infrastruktur-TLP-CLEAR.pdf).  
_Angrebet skete i maj 2023. Udgivelsen er fra november 2023._  

Rapporten er spændende/uhyggelig læsning om et sofistikeret angreb samt hvordan de enkelte energiselskaber var klædt på til angrebet. Heldigvis lykkedes det at stoppe angrebet før der blev gjort skade, men det kunne være sket.  

### Lovgivning 2: Net- og Informationssikkerhedsdirektivet (NIS2)  

Der er altså god grund til at sikkerheden omkring kritisk infrastruktur bør forbedres og derfor indføres NIS2 direktivet i EU.  

Det forventes at træde i kraft til januar. Lovgivningen er forsinket i danmark og burde træde i kraft 17. oktober 2024.  
Implementation af lovgivningen i danmark vil nok ligne [Udkast til lovforslag](https://prodstoragehoeringspo.blob.core.windows.net/535dd4d9-0b77-4d4c-a977-7afdfc82b1c7/Udkast%20til%20forslag%20til%20lov%20om%20foranstaltninger%20til%20sikring%20af%20et%20h%C3%B8jt%20cybersikkerhedsniveau.pdf).  

NIS2 er den almindelige betegnelse for EU-direktiv (EU) 2022/2555, som fastlægger foranstaltninger til sikring af et højt fælles cybersikkerhedsniveau i EU.  
NIS2-direktivet bygger ovenpå og ophæver EU-direktivet om sikkerhed i net- og informationssystemer (NIS1-direktivet).  

Formålet med NIS2-direktivet er at yderligere styrke og ensarte cybersikkerheden og modstandsdygtigheden overfor cybertrusler på tværs af EU for virksomheder inden for en lang række sektorer og for offentlige myndigheder, som anses for at være kritiske for økonomien og samfundet.  

Med NIS2 indføres en række nye krav til omfattede virksomheder og myndigheder. NIS2 stiller bl.a. krav om gennemførelse af cybersikkerhedsforanstaltninger, hændelsesrapportering samt giver styrkede tilsyns- og håndhævelsesbeføjelser.  

Flere myndigheder og virksomheder (kaldet ”enheder”) vil være omfattet, end tilfældet var med det hidtidige NIS-direktiv (NIS1).  

De sektorer, som er omfattet af NIS2-direktivet, er beskrevet i direktivets bilag 1 (sektorer af særligt kritisk betydning) og bilag 2 (andre kritiske sektorer).  
_[https://www.cfcs.dk/da/om-os/nis2/](https://www.cfcs.dk/da/om-os/nis2/)_  

[Dansk Standard](https://www.ds.dk/da/i-fokus/lovgivning/lovgivning-paa-det-digitale-omraade-og-standarder/nis2-direktivet-net-og-informationssikkerhedsdirektivet) skriver også om NIS2-direktivet, læg mærke til at her nævnes at NIS2-direktivet opfordrer til at anvende internationale, anerkendte standarder (artikel 21 og 25).  

I videregående Governance skal i nok regne med at arbejde en del med NIS2. Dels fordi underviseren ved ret meget om NIS2 men også fordi det er en god måde at arbejde med ISO27000 standarden.  

## Problem 3: Ikke sikker omgang med persondata

Der findes desværre alt for mange eksempler på omgang med persondata på en ikke sikker måde. Det er dog heller ikke mage år siden at persondatabeskyttelse var en by i et vist østliggende land.  

Der har været mange sager i pressen om kommuner og private virksomheder der har lækket persondata.  

Et eksempel er familieretshuset se: [Familieretshuset får igen alvorlig kritik ](https://www.datatilsynet.dk/presse-og-nyheder/nyhedsarkiv/2024/aug/familieretshuset-faar-igen-alvorlig-kritik)  
         
Familieretshuset har anmeldt 28 brud på persondatasikkerheden til Datatilsynet i perioden fra den 6. januar 2023 til den 30. juni 2024.  
Bruddene består i, at Familieretshuset uberettiget har videregivet oplysninger om beskyttede navne og adresser eller andre oplysninger, som potentielt kan afsløre en persons opholdssted, herunder ophold på krisecenter.  
Oplysningerne blev i de fleste tilfælde videregivet til den anden part i sagen, som typisk kunne være årsagen til valget om navne- og adressebeskyttelse eller ophold på krisecenter. _datatilsynet_

Et andet eksempel er Østrigeren Maximillian Schrems. Han er ophavsmand til flere europæiske sager om personoplysninger.  
Schrems II-sagen medførte blandt andet at overførsel af personoplysninger til USA ved brug af Privacy-Shield ikke kan ske.  
I praksis opfattes USA efter sagens afgørelse som usikkert tredjeland.   
Læs mere her: [EU-Domstolens dom i Schrems II-sagen ](https://www.datatilsynet.dk/presse-og-nyheder/nyhedsarkiv/2020/jul/eu-domstolens-dom-i-schrems-ii-sagen)

Maximillian Schrems står i spidsen for organisationen [**None Of Your Business** (NOYB)](https://noyb.eu/en) og har senest åbnet en sag mod openAI's "hallucinationer" der medfører urigtige personoplysninger der ikke kan berigtiges:  
[Schrems NGO files GDPR complaint against OpenAI over AI ‘hallucinations’](https://www.euractiv.com/section/digital/news/schrems-ngo-files-gdpr-complaint-against-openai-over-ai-hallucinations/)  

### Lovgivning 3: GDPR  

Der er altså mange gode grunde til at GDPR (General Data Protection Regulation) eller oversat til dansk, databeskyttelsesforordningen, eksisterer.  

Nu er det jeres tur til at udforske GDPR lidt på egen hånd :-)

## Instruktioner

1. Læs alle informationer herover, vær nysgerrig og undersøg links. Snak om hvad i finder undervejs, hvad er nyt? Er der noget i bliver overraskede over eller??
2. Læs om GDPR ved hjælp af nedenstående links.
3. Besvar i gruppen følgende spørgsmål
    1. Hvem er beskyttet af GDPR?
    2. Hvor mange kapitler er der i GDPR?
    3. Hvor mange artikler er der i GDPR?
    4. Hvad definerer artikel 4 som personoplysninger?
4. Besøg [datatilsynet](https://www.datatilsynet.dk/) og gennemse de seneste nyheder for at se, hvad der sker aktuelt.
5. Tænk på en organisation, som du har godt kendskab til (f,eks. en undervisningsinstitution, en virksomhed eller en offentlig institution)  
  Diskuter følgende spørgsmål med udgangspunkt i den konkrete organisation.
    1. Hvilke persondata indsamles der i den givne organisation?
    2. Hvem er _den registrerede_, når organisationen indsamler persondata?
    3. Hvem er _dataansvarlig_, når organisationen indsamler persondata?
    4. Hvem er _databehandler_, når organisationen indsamler persondata?
    5. Hvem er _ledende tilsynsmyndighed_, når organisationen indsamler persondata?
6. Vi afrunder i fællesskab med opsamling på klassen 

## Links

- [Grundlæggende for GDPR](https://www.datatilsynet.dk/hvad-siger-reglerne/vejledning/gdpr-univers-for-smaa-virksomheder/grundlaeggende-om-gdpr)
- [General Data Protection Regulation (GDPR/databeskyttelsesforordningen)](https://gdpr.dk/databeskyttelsesforordningen/)