---
hide:
  - footer
---

# Øvelse 4 - Grundlæggende cyberforsvar

## Information

I de næste 7 uger skal i arbejde med sikkerhed i IT-Governance. Det er for mange af jer et helt nyt fagområde med mange nye begreber og det kan være svært at få et overblik. 

_"Center for Cybersikkerhed hjælper myndigheder og virksomheder med at ruste sig mod de trusler, der følger med digitaliseringen. CFCS rådgiver om truslerne og om de organisatoriske og tekniske foranstaltninger, der er med til at øge sikkerheden. Når uheldet er ude, kan myndigheder og virksomheder, der beskæftiger sig med samfundsvigtige opgaver, kontakte CFCS' Situationscenter."_  
- https://www.cfcs.dk/da/om-os/

CFCS har lavet en kondenseret vejledning som myndigheder og virksomheder kan
bruge til at opbygge et grundlæggende cyberforsvar.  
Den indeholder mange gode begreber og et fornuftigt grundsyn på opbygning af et godt forsvar med både organisatoriske og tekniske foranstaltninger.

Formålet med denne øvelse er at i læser og undersøger hvad det indebærer at arbejde med at etablere et grundlæggende cyberforsvar.

Som en hjælp har jeg lavet nedenstående spørgsmål. I næste uge bruger vi jeres svar til dialog på klassen.  
Her taler vi om hvad i har læst og især hvad der var lidt svært at forstå.  
Hvis i får nogle "aha" oplevelser må i meget gerne tage dem med også :-)

Det er vigtigt at i noterer jeres svar for hvert spørgsmål og deler dem med alle i jeres gruppe.  
Husk at skrive spørgsmålet ovenfor svaret så i har konteksten med.

## Instruktioner

1. Hent CFCS vejledningen [Cyberforsvar der virker](https://www.cfcs.dk/da/forebyggelse/vejledninger/cyberforsvar-der-virker/)
2. Læs vejledningen og besvar følgende spørgsmål skriftligt:
    1. Hvad er ledelsens rolle?
    2. Hvilke værktøjer kan ledelsen benytte sig af?
    3. Hvorfor skal ledelsen stille spørgsmål til sig selv?
    4. Hvorfor skal ledelsen still spørgsmål til sin organisation?
    5. Hvilke af de 10 tekniske tiltag ville i starte med, hvis i skulle implementere et Cyberforsvar? og hvorfor?
    6. Hvilke standarder og best practices omtales der i vejledningen?
    7. Er tekniske tiltag vigtigere en adfærdstiltag?
    8. Vil en virksomhed være sikret mod trusselsaktører (ondsindede hackere/aktører) hvis de følger vejledningen?
    9. Hvilke faser indgår der i krisehåndteringscyklussen?
    10. Hvordan kan en virksomhed afprøve deres cyberforsvar?


## Links

- [CFCS vejledninger](https://www.cfcs.dk/da/forebyggelse/vejledninger/)
- [Sikker Digital](https://www.sikkerdigital.dk/)
