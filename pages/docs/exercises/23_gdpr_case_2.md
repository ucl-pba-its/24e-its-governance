---
hide:
  - footer
---

# Øvelse 23 - GDPR i virkeligheden 2: Familieretshuset

## Information

Familieretshuset hed tidligere statsforvaltningen. Familieretshuset behandler sager med meget personfølsomme oplysninger, i sager der handler om brud og overgange i familien. Det kan være alt fra skilsmisse til adoption og afgørelse om forældremyndighed.  
Man kan sige at familieretshuset er et virkelig godt eksempel på at GDPR har sin berettigelse. 

Familieretshuset har gentagne gange lækket personfølsomme data med risiko for alvorlige konsekvenser for de berørte.  

Men hvorfor er en statslig organisation ikke bedre til at passe på personfølsomme oplysninger og hvad mener i om dilemmaet i, at en statslig organisation gentagende gange kan overtræde loven, tilsyneladende ustraffet?

**Vi samler op på klassen efter i har arbejdet med øvelsen.**

## Instruktioner

1. Læs de artikler om familieretshuset som er i dagens plan på itslearning.  
De er i mappen som hedder *Familieretshuset artikler*. Læs dem i kronologisk rækkefølge
2. Læs om [familieretshuset](https://familieretshuset.dk/) - de har blandt andet en artikel om deres indberettede databrud og datatilsynets kritik.
3. Hvor mange databrud har familieretshuset indberettet siden januar 2023? Cirka....
4. Hvilke konsekvenser kan databruddene have for de berørte borgere?  
Giv et par eksempler baseret på artiklerne og familiretshusets hjemmeside
5. Hvordan opstår de fejl som fører til databrud i familiretshuset? 
6. Familiretshuset er ikke blevet straffet med bøde. Hvorfor ikke, ifølge datatilsynet?
7. Hvilket tiltag er familieretshuset igang med at implementere ifølge artiklerne?  
Er tiltaget organisatorisk eller teknisk?
8. Hvad ville i gøre hvis i skulle hjælpe familieretshuset med at overholde GDPR?

## Links

- [Om familieretshuset](https://familieretshuset.dk/om-familieretshuset/om-familieretshuset)
- [GDPR.DK](https://gdpr.dk)
- [Europa-Parlamentets og Rådets forordning (EU) 2016/679 af 27. april 2016](https://eur-lex.europa.eu/legal-content/DA/TXT/PDF/?uri=CELEX:32016R0679)
- [Databeskyttelsesloven - LBK nr 289 af 08/03/2024](https://www.retsinformation.dk/eli/lta/2024/289)