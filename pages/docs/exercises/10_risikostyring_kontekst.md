---
hide:
  - footer
---

# Øvelse 10 - Risikostyring: Etablering af kontekst

## Information

![Risikostyringsprocessen1](../images/risikostyringsprocessen1.drawio.png){ width="100%" }  
_Risikostyringsprocessens 5 trin ifølge ISO/IEC 27005_

I dette trin etableres den kontekst der gælder for den givne virksomhed. Kontekst er mange ting.
Det er:  

- Afdækning af virksomhedens interne og eksterne interessenter
- Beslutning om virksomhedens tilgang til risikostyringsprocessen  
- Hvad er virksomhedens formål (produkter, etik, politikker osv.) 
- Hvad betyder informations- og cybersikkerhed i virksomheden? 
- Hvilke systemer og informationer er særligt vigtige at beskytte?  

I etableringen af kontekst er der en række primære aktiver samt en række understøttende aktiver der identificeres.    
Forretningskritiske processer, informationer og viden kaldes _primære aktiver_.  
Til disse vil der være en række _understøttende aktiver_ som understøtter de primære aktiver, eksempelvis IT-udstyr, software, hardware, personale og fysiske lokationer.  

Virksomheden vælger om de vil anvende kvalitativ eller kvantitativ metode til at udregne risiko og kriterier for identifikation, analyse og
evaluering af risici opstilles. Virksomheden definerer også kriterierne for, hvornår en risiko kan accepteres (risikoappetit).

## Instruktioner

1. Etabler en kontekst for jeres virksomhed, brug information og eksempler på side 13-18 i _Guide til risikostyring (Dansk Standard DS/INF 10017:2023)_  
Jeres kontekst skal indeholde:
    - Virksomhedens karateristika
    - Identifikation af iteressentlandskabet og interessenters forventninger
    - Identifikation af forretningsprocesser
    - Tilgang til at udregne risiko (kvantitativ eller kvalitativ)
    - Fastsættelse af niveauer for konsekvens og sandsynlighed og hvad de betyder.
    - Hvornår kan en risiko accepteres?
    - Definition af kriterier for risikoejer
2. Beskriv konteksten på samme måde som eksemplerne på side 16-18 i _Guide til risikostyring (Dansk Standard DS/INF 10017:2023)_  
Det er vigtigt at i konstruerer tabeller for Konsekvens og sandsynlighed samt opstiller disse i en risiko matrix som for eksempel tabel A3, B3 eller C3. Skalaerne for hhv. sandsynlighed og konsekvens skal vælges ud fra jeres tilgang til at udregne risiko.

## Links

- ...