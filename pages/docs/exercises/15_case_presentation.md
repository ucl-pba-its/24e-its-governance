---
hide:
  - footer
---

# Øvelse 15 - Virksomhedscase: Præsentation

## Information

**Dette er en individuel øvelse** 

Den fiktive virksomhed _**Sentinel Surveillance Systems**_ er omdrejningspunktet for øvelse 15 - 20.   
Du har allerede mødt virksomhedscases i arbejdet med risikostyring og guiden fra dansk standard.  

Formålet med denne øvelse er at blive godt bekendt med casen så du/i har et godt afsæt til at arbejde med den. 

## Instruktioner

1. Hent virksomhedscasen fra itslearning - dokumentnavn "Case Sentinel Surveillance Systems.pdf"
2. Læs casen
3. Opret et dokument i deler i gruppen og som i kan bruge til at skrive ting ned i arbejdet med casen.
3. Skriv de ting fra casen ned som du tænker relaterer til sikkerhed, f.eks:
    - Virksomheds størrelse
    - Hvad producerer virksomheden?
    - Digitaliseringsgrad
    - Virksomhedens omdømme
    - Geografi
    - Udviklingsmiljøer?
    - Virksomhedens afdelinger
    - Sektorer
4. Gør dig bekendt virksomhedens risikokriterier, risikovillighed og risiko niveau
5. Hvilken del af risikostyringsprocessen repræsenterer virksomhedscasen? Skriv det ned.
6. Vi taler om casen på klassen som opsamling på øvelsen.

## Links

- ...