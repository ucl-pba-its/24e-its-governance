---
hide:
  - footer
---

# Øvelse 4 svar - Grundlæggende cyberforsvar

## Information

Dette er undervisers svar på [Øvelse 4 - Grundlæggende cyberforsvar](../exercises/4_basic_cyberdefense.md).

Svarene er givet som bedste bud, det vil sige at det ikke nødvendigvis er et facit men snarere undervisers opfattelse af svar.  

I kursiv angives reference til hvilken del af vejledningen er svaret refererer til.

## Instruktioner


1. **Hvad er ledelsens rolle?**  
    Topledelsens bevågenhed er den vigtigste forudsætning for et cyberforsvar der virker. 
    Ledelsen skal styre cyber- og informationssikkerheden ved løbende at støtte, prioritere og følge op på målsætninger og strategier efter samme principper som inden for f.eks. økonomi og HR.  
    Et effektivt cyberforsvar er ikke et projekt, men en løbende proces, hvor der hele tiden skal evalueres og optimeres. Det gælder for alle seks trin i denne vejledning. Derfor skal ledelsen prioritere løbende opfølgning og forbedring.  
    (_Cyberforsvar der virker, s. 3_)
2. **Hvilke værktøjer kan ledelsen benytte sig af?** 
    - ledelsen skal styre cyber- og informationssikkerhedsområdet
    - Som leder er det vigtigt at forstå cybertruslen
    - Topledelsen skal tage ejerskab for myndighedens eller virksomhedens målsætninger og strategier på cyber- og informationssikkerhedsområdet.
    - Ledelsens strategier og målsætninger skal udmøntes i politikker, procedurer og retningslinjer. 
    - 16 spørgsmål til organisationen
3. **Hvorfor skal ledelsen stille spørgsmål til sig selv?**  
    - Svarene på spørgsmålene, kan være med til at give ledelsen et indtryk af, hvordan organisationen arbejder med cyber- og informationssikkerhed.
4. **Hvorfor skal ledelsen still spørgsmål til sin organisation?**
    - Svarene på spørgsmålene vil være med til at synliggøre for ledelsen, hvis der er områder i organisationen eller hos ledelsen selv, der trænger til at blive prioriteret.
5. **Hvilke af de 10 tekniske tiltag ville i starte med, hvis i skulle implementere et Cyberforsvar? og hvorfor?**
    - Det er ikke muligt at svare på uden at kende kontekst, altså virksomhedens aktiver og eksisterende forsvar.  
    Uden kendskab til det vil det være intuitivt at gøre det det koster mindst, først. Men det vil være som at arbejde i blinde uden at vide om virksomheden reelt opnår en højere sikkerhed. 
6. **Hvilke standarder og best practices omtales der i vejledningen?**
    - [ISO27001](https://www.ds.dk/da/om-standarder/ledelsesstandarder/iso-27001-informationssikkerhed)
    - [NIST Cybersecurity Framework](https://www.nist.gov/cyberframework)
    - [SANS](https://www.sans.org/nice-framework/)
    - ITIL (best practice for IT Service Management [Axelos](https://www.axelos.com/certifications/itil-service-management/itil-4-foundation))
    - CIS18 (Center for internet security [CIS18 controls](https://controls-assessment-specification.readthedocs.io/en/stable/index.html))   
    (_Cyberforsvar der virker, s. 3 og 8_)
7. **Er tekniske tiltag vigtigere en adfærdstiltag?**  
      Nej. Teknikken skal udgøre et stærkt bolværk mod cyberangreb – det er med andre ord teknikken, der hjælper os.  
      Teknikken kan dog ikke stå alene. Medarbejdernes adfærd skal udgøre et ligeså stærkt bolværk mod cyberangreb som teknikken.  
      (_Cyberforsvar der virker, s. 9_)
8. **Vil en virksomhed være sikret mod trusselsaktører (ondsindede hackere/aktører) hvis de følger vejledningen?**  
      Nej, det er ikke muligt at være sikker. I sikkerhed arbejdes med begrebet ["assume breach"](https://www.f5.com/company/blog/zerotrust-three-key-strategic-components-of-assume-breach) som betyder at sikkerhedsarbejdet skal udføres med en grundantagelse om at der vil ske eller allerede er sket en kompromittering af virksomhedens sikkerhed.  
      I vejledningen under kapitlet om kriseberedskab nævnes:  
      Uanset hvor mange tekniske og organisatoriske tiltag, der er implementeret, og uagtet hvor godt et uddannelsesprogram der kører, så er det kun et spørgsmål om tid, før jeres myndighed eller virksomhed rammes af et cyberangreb.  
      (_Cyberforsvar der virker, s. 13_)
9. **Hvilke faser indgår der i krisehåndteringscyklussen?**
    - Isolering: Afgrænsning og isolering af årsagen til driftsforstyrrelsen.
    - Eliminering: Fjernelse og eliminering af forstyrrelsen
    - Reetablering: Reetablering af de berørte systemer og netværk  
    (_Cyberforsvar der virker, s. 15_)
10. **Hvordan kan en virksomhed afprøve deres cyberforsvar?**  
      Løbende gennemføre undersøgelser af de eksisterende sikkerhedstiltag, der er implementeret af myndigheden eller virksomheden.
    - Sårbarhedsscanninger 
    - Pentests
    - Egenkontrol
    - Audit 
    - Revision
    - målinger eller tests – eksempelvis en phishingtest  
    (_Cyberforsvar der virker, s. 16 og 17_)


## Links

- [CFCS vejledninger](https://www.cfcs.dk/da/forebyggelse/vejledninger/)
- [Sikker Digital](https://www.sikkerdigital.dk/)