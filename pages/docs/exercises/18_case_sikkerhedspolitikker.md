---
hide:
  - footer
---

# Øvelse 18 - Virksomhedscase: Sikkerhedspolitikker

Dette er en gruppeøvelse.

## Information

Som bekendt beskriver ISO/IEC 27001 krav til etablering af et ledelsessystem for informationssikkerhed (**ISMS** - Information Security Management System).  

Et ISMS indbefatter alle de politikker, procedurer, retningslinjer og tilhørende ressourcer og aktiviteter, som en organisation administrerer for at beskytte sine informationsaktiver.  

En central komponent i et ISMS er informationssikkerhedspolitikken. Det er et dokument der beskriver de overordnede rammer for en virksomheds prioriteter ifht. informationssikkerhed (rammer, organisation, fordeling af ansvar og målsætning).  

Under dokumentet kan være en eller flere emnespecifikke underpolitikker. Det kan være underpolitikker for _Adgangsstyring_, _kommunikationssikkerhed_, _leverandør forhold_, _beredskabsplan_ og meget mere.  

Disse dokumenter kan præsenteres som et _dokumenthieraki_ som det gøres i [vejledningen for informationssikkerhedspolitik](https://www.sikkerdigital.dk/Media/638430664323679104/Vejledning-i-politikker-for-informationssikkerhed-aug2021.pdf) fra sikker digital.

![dokumenthieraki_vejledning_politikker_informationssikkerhed_digitaliseringsstyrelsen.png](../images/dokumenthieraki_vejledning_politikker_informationssikkerhed_digitaliseringsstyrelsen.png)

At der udarbejdes en informationssikkerhedspolitik er et ledelsesansvar. Politikken skal afstemmes med eksterne krav som for eksempel lovgivning og samarbejdspartnere.

Formålet med denne øvelse er at opnå viden om indholdet i en sikkerhedspolitik.  
Dette gøres ved hjælp af at arbejde med sikker digitals [eksempel på informationssikkerhedspolitik](https://www.sikkerdigital.dk/Media/637963161536561731/informationssikkerhedspolitik.pdf)

## Instruktioner

1. Find de afsnit i hhv. ISO/IES 27001 og 27002 der omhandler sikkerhedspolitik. Læs hvad der står i disse afsnit. 
3. Skimlæs [eksempel på informationssikkerhedspolitik](https://www.sikkerdigital.dk/Media/637963161536561731/informationssikkerhedspolitik.pdf) fra sikker digital.
2. I [eksempel på informationssikkerhedspolitik](https://www.sikkerdigital.dk/Media/637963161536561731/informationssikkerhedspolitik.pdf) fra sikker digital udvælger i 2 eller flere afsnit i vil tilrette så det passer med de foranstaltninger i valgte i [øvelse 17](../exercises/17_case_risk_handle.md)
4. Opret et dokument baseret på [eksempel på informationssikkerhedspolitik](https://www.sikkerdigital.dk/Media/637963161536561731/informationssikkerhedspolitik.pdf) som et udgangspunkt for en sikkerhedspolitik til _**Sentinel Surveillance Systems**_ - de afsnit i har tilrettet skal selvfølgelig med som erstatning for de generiske afsnit fra eksemplet.

## Links

- [Sikker digital - Politik for informationssikkerhed](https://www.sikkerdigital.dk/myndighed/iso-27001-implementering/forstaa-arbejdet-med-informationssikkerhed/politik)

Links til diverse it-sikkerhedspolitikker:    

- [Læsø Forsyning A/S](https://www.laesoeforsyning.dk/praktisk-information/it-sikkerhedspolitik/)
- [Sagro](https://www.sagro.dk/om-os/sagro-viden-og-vaekst/persondatapolitik-og-it-sikkerhedspolitik)
- [Sosu Esbjerg](https://www.sosuesbjerg.dk/media/3j5jiz3j/it-sikkerhedspolitik-hos-sosu-esbjerg-ver-15.pdf)
- [Nykredit](https://www.nykredit.com/globalassets/nykredit.com/pdf/it-sikkerhedspolitik.pdf)
- [Slagelse kommune](https://www.slagelse.dk/media/accbusos/informationssikkerhedspolitik.pdf)
- [Greve Kommune](https://greve.dk/media/wpjlqxhc/it-sikkerhedspolitik-2013.pdf)