---
hide:
  - footer
---

# Øvelse 21 - GDPR i detaljer

## Information

Denne øvelse dykker dybere i GDPR og kan hjælpe jer til at få kigget nærmere på GDPR og de principper forordningen bygger på. 

I bør notere jeres overvejelser, drøftelser og svar i et fælles dokument, mange af de ting i undersøger vil være direkte anvendelige til eksamen.

**Vi samler op på klassen efter i har arbejdet med øvelsen.**

## Instruktioner

Brug kilderne (links) herunder til at undersøge GDPR og besvare følgende Spørgsmål:  

1. Hvilket territorialt anvendelsesområde har GDPR?
2. Hvilke rettigheder har de registrerede i persondataforordningen?   
3. Hvad er en dataansvarlig ifølge GDPR?
4. Hvad er en databehandler ifølge GDPR?
5. Hvad menes med ansvarlighed (Artikel 5 - ansvarlighedsprincippet)
6. Hvad menes med tilstrækkelighed (proportionalitetsprincippet - Artikel 5)
7. Hvad menes med passende foranstaltninger (både organisatorisk og teknisk)
8. Indsigt (hvad menes der med det?)
9. Dataetik og GDPR, hvordan hænger det sammen? (Se link nederst i øvelsen)
10. Hvilken aldersgrænse er implementeret i dansk logivning ifht. artikel 8. stk. 1?
11. Hvad er særlige kategorier af personoplysninger?

**BONUS HVIS TID**

1. Brud på persondatasikkerheden. Hvornår er der sket et brud?
2. Brud på persondatasikkerheden. Hvornår skal det indberettes til myndighederne?
3. Brud på persondatasikkerheden. Hvornår skal de registrerede underrettes?
4. Brud på persondatasikkerheden. Hvad skal underretningen indeholde og hvad er tidsfristerne?
5. Brud på persondatasikkerheden. Hvad er tidsfristerne?

## Links

- [GDPR: Hvad er sammenhængen mellem dataetik og persondataretten?](https://www.wiredrelations.com/viden/gdpr-hvad-er-sammenhaengen-mellem-dataetik-og-persondataretten)
- [GDPR.DK](https://gdpr.dk)
- [Europa-Parlamentets og Rådets forordning (EU) 2016/679 af 27. april 2016](https://eur-lex.europa.eu/legal-content/DA/TXT/PDF/?uri=CELEX:32016R0679)
- [Databeskyttelsesloven - LBK nr 289 af 08/03/2024](https://www.retsinformation.dk/eli/lta/2024/289)
- [Proportionalitetsprincippet - stadsrevisionen.dk](https://stadsrevisionen.dk/blog/proportionalitetsprincippet/)