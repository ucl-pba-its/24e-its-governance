---
hide:
  - footer
---

# Øvelse 14 - Risikostyring: Håndtering af risici

## Information

![Risikostyringsprocessen5](../images/risikostyringsprocessen5.drawio.png){ width="100%"}  
_Risikostyringsprocessens 5 trin ifølge ISO/IEC 27005_

Nu har i identificeret, analyseret og evalueret de enkelte risici. I ved hvilke risici ledelsen vil have jer til at håndtere ud fra ledelsens risikovillighed.  
Næste skridt i processen er at vurdere hvordan de enkelte risici skal håndteres.  

Her er der reelt fire muligheder:

1. **Acceptere risikoen:** Der er så simplet som det lyder, i vælger ikke at gøre noget. Det kan skyldes at det bliver for dyrt at håndtere risikoen, at det ikke er muligt at gøre noget (meteornedslag??). Det lyder måske ligegyldigt men faktisk er fordelen at i nu er bevidste om at risikoen eksisterer og kan addresere den senere hvis det bliver nødvendigt.
2. **Undgå risikoen:** Det gøres ved at stoppe eller ændre den aktivitet der er årsag til risikoen. For eksempel at slukke for den sårbare server eller fjerne personfølsom data fra et system.  
3. **Flytte/dele risikoen:** For eksempel ved at få nogle andre til at drifte aktiviteten eller købe en forsikring.
4. **Forøge/minimere risikoen:** ved at implementere foranstaltninger der mindsker sandsynlighed og/eller konsekvens. Det kan være kryptering osv. ISO/IEC 27002:2022 har en række af sikkerhedsforanstaltninger der kan anvendes som inspiration.  
Det lyder måske ikke så smart at forøge en risiko, men nogle gange gøres det ud fra et forretningshensyn. Her accepteres en større risiko fordi gevinsten ved satset er for stort til at undlade risikoen. Et eksempel er at indtræde på et nyt marked og de risici der følger med det.


## Instruktioner

Brug information og eksempler på side 27-28 i _Guide til risikostyring (Dansk Standard DS/INF 10017:2023)_ når i arbejder.   

1. For hver risici der skal håndteres vælger i hvordan det skal udføres (accepteres, undgås, flyttes/deles, forøges/minimeres)
2. For hver risici udvælger i foranstaltninger der understøtter håndteringen ud fra listen af foranstaltninger i ISO/IEC 27002:2022.
3. Beskriv jeres håndtering af de enkelte risici, i kan bruge eksemplerne i guiden som inspiration (side 28)

## Links

- ...