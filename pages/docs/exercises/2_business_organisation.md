---
hide:
  - footer
---

# Øvelse 2 - Organisationsforståelse

## Information

Virksomheder og organisationer kan være meget forskellige.  
Størrelsen på virksomheder omtales på forskellige måder.

Det kan som eksempel være efter:

- [regnskabsklasser](https://erhvervsstyrelsen.dk/krav-til-aarsrapportens-indhold)
- [D-Mærkets virksomhedsgrupper](https://www.d-maerket.dk/bliv-d-maerket/virksomhedsgrupper)
- [CIS Implementation Groups](https://www.cisecurity.org/controls/implementation-groups)

Måden virksomhederne er indrettet på i forhold til virksomhedens afdelinger og ansvarområder skitseres som regel, i hvert fald hvis det er en større virksomhed, i et organisationsdiagram.

Et organisationsdiagram er en visuel oversigt, der viser, hvordan en virksomhed eller organisation er struktureret. Det illustrerer, hvem der har hvilke roller, hvem der rapporterer til hvem, og hvordan forskellige afdelinger og medarbejdere er forbundet.

Organisationer tildeler titler til de forkellige ledelses roller og afdelinger, som ekesmpel bruges nedenstående:

| Titel eller afdeling | Forkortelse | 
|----------|--------------| 
| Chief Executive Officer | CEO | 
| Chief Technology Officer | CTO | 
| Chief Information Officer | CIO | 
| Chief Operating Officer | COO | 
| Chief Financial Officer | CFO | 
| Vice President | VP | 
| Quality Assurance | QA | 
| Human Resources | HR |

Formålet med denne øvelse er at få en forståelse for hvordan en virksomhed typisk indretter sig hierakisk samt at i identificerer de roller i virkosmheden der er centrale i arbejdet med sikkerhed.

## Instruktioner

1. Find mindst et organisationsdiagram der inkluderer en it afdeling. Det kan være svært at finde et godt eksempel.
2. På diagrammet skal i prøve at udpege hvilke ledelsesroller der er ansvarlig for it-sikkerheden i virksomheden. 
3. Skriv titler for de forskellige roller samt hvad deres ansvarsområde typisk dækker. 
4. Skriv jeres svar i **sektion 1** i denne padlet [https://padlet.com/3ksdwqjwep/hvad-er-it-governance-c939zrgbl67q8k0t](https://padlet.com/3ksdwqjwep/hvad-er-it-governance-c939zrgbl67q8k0t)   
5. Som afrunding taler vi på klassen om jeres svar, formålet er at opnå en fælles forståelse blandt alle i klassen.

## Links
