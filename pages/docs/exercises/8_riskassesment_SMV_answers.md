---
hide:
  - footer
---

# Øvelse 8 - Risokovurdering i SMV (Forberedelse til undervisningsgang 3)

## Information

Dette er undervisers svar på [Øvelse 8 - Risokovurdering i SMV (Forberedelse til undervisningsgang 3)](../exercises/8_riskassesment_SMV.md).

Svarene er givet som bedste bud, det vil sige at det ikke nødvendigvis er et facit men snarere undervisers opfattelse af svar.  

I kursiv angives reference til hvilken del af vejledningen er svaret refererer til.

## Instruktioner

- Hvad skal udføres (mere eller mindre instruerende)
- Hvad skal det bruges til (fremlæggelse, aflevering etc.)

## Links

- Links til flere informationer som kan bruges til at studere øvelsens emne yderligere