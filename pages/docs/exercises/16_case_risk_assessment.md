---
hide:
  - footer
---

# Øvelse 16 - Virksomhedscase: Risikovurdering

## Information

**Dette er en gruppeøvelse**  

Konteksten for _**Sentinel Surveillance Systems**_ er, som du fandt ud af i [Øvelse 15](../exercises/15_case_presentation.md), allerede etableret. 

Formålet med denne øvelse er at repetere risikovurderingsprocessen som i arbejdede med i forrige undervisningsgang.  
Et andet formål er at risikovurderingen skal anvendes når vi skal arbejde med sikkerhedspolitikker.

Hvis i har brug for det kan i genbesøge øvelserne 11, 12 og 13 som tilsammen udgør de 3 processer i risikovurdering, _identifikation_, _analyse_ og _evaluering_.

Når i arbejder kan i også bruge:  

- _Guide til risikostyring (Dansk Standard DS/INF 10017:2023)_
- ISO/IEC 27005:2022

Husk at jo flere gange i bruger ovennævnte kilder jo bedre bliver i til at anvende og navigere i dem.  

## Instruktioner

**Risko identifikation:**  

1. Benyt både den hændelsesbaserede og aktivbaserede tilgang til at identificere risici. (selvfølgelig ud fra virksomhedens kontekst)
2. Identificer så mange risici som muligt - noter også hvilket aktiv eller hændelse de tilhører. I skal være opsøgende og meget gerne tage udgangspunkt i kendte trusler og underbygge jeres identifikation med kilder fra internettet og/eller de links til MITRE og NIST jeg angiver herunder. Et eksempel er som svarene på [øvelse 5](../exercises/5_trusselsbillede_overview_answers.md)
3. Beskriv hvem der er ansvarlig for de enkelte risici, brug de ledelsestitler i arbejdede med i [øvelse 2](../exercises/2_business_organisation.md)
4. Lav en liste over identificerede risici med ejere tilknyttet.

**Risko analyse:**  

1. Brug information og eksempler på side 22-23 i _Guide til risikostyring (Dansk Standard DS/INF 10017:2023)_ når i arbejder.  
2. Tal i gruppen om hver af jeres identificerede risici og fastslå konsekvens samt sandsynlighedsniveau for hver risici.
3. Beskriv hver risici på samme måde som eksemplerne i guiden (side 23)

**Risiko evaluering:**  

1. Prioritér listen med risici i forhold til sandsynlighed og konsekvens med angivelse af niveau fra jeres risikomatrix.
2. Opdel listen i de risici der kan accepteres (grøn) og de risici der bør nedbringes (gul og rød).  
3. Opdel de risici der bør nedbringes i hhv. risici der kan accepteres af produktejer og de risici der skal accepteres på direktionsniveau.

## Links

- [MITRE ATT&CK](https://attack.mitre.org/)
- [MITRE ATT&CK Groups](https://attack.mitre.org/groups/)
- [NIST Vulnerability Database](https://nvd.nist.gov/vuln/search)
- CIA model [https://medium.com/learn-and-grow/what-is-the-cia-model-ae160bac21f](https://medium.com/learn-and-grow/what-is-the-cia-model-ae160bac21f)
- CIA & DAD model [https://medium.com/@yadavtejas249/cia-and-dad-triad-ef84a94f9aee](https://medium.com/@yadavtejas249/cia-and-dad-triad-ef84a94f9aee)