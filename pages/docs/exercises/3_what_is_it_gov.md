---
hide:
  - footer
---

# Øvelse 3 - Hvad er IT-Governance?

## Information

Denne øvelse er en gruppe øvelse.

I 1960erne begyndte IT at være en del af virksomheder. Dengang var IT et meget specialiceret område, meget mere end det er i dag. Ofte forstod forretningsdelen af virksomheden ikke så godt hvordan IT virkede og derfor var det også svært som leder at styre virkosomehdes it-ressourcer.  
I starten var IT afdelingernes budget en mindre del af virksomheden. De skabte værdi ved at blandt andet at kunne automatisere og effektivisere arbejdsgange. Nogen gange kunne IT vise sig at åbne op for en helt ny indtjeningsmulighed i virksomheden.    

Omkostningerne til IT blev gennem årene gradvist forøget men stadig med en ringe forståelse fra resten af virksomheden af hvordan det blev udviklet og implementeret. Man kan sige at IT afdelingen havde forholdsvist frie hænder til at gøre hvad de vurderede var det bedste for virksomheden. 
Problemet var bare at IT specialister som oftest ikke har de bedste forretningskompetencer og derfor tog beslutninger der på sigt belastede virkosmhederne ved store omkostninger og fastlåsning i rigide it systemer.  

Svaret fra forretningen blev at begrænse IT budgetterne, men desværre stadig med den ringe indsigt i IT som var med til at skabe problemerne.  

Konflikten lever stadig i bedste velgående. Ofte oplever det ikke tekniske personale i virkosmheder en irritation over it-systemer, herunder de mange nye sikkerhedstiltag der tages i disse år (MFA, Kryptering, VPN, etc.)  
Man kan sige at der er skabt en "dem og os" situation i virksomheden der ikke er gavnlig for forretningen.  
Det er en kompleks problemstilling som der ikke er nogen nem løsning på, men det bedste bud er måske at arbejde struktureret med it-governance.

Formålet med denne øvelse er at i får en forståelse af hvad it-governance betyder og er. Det er helt essentiel for at kunne forstå de emner vi skal arbejde med i resten af faget.  
Det er især vigtigt hvis du ikke har den store erhvervserfaring og derfor skal forstå konteksten teoretisk.  

## Instruktioner

1. Undersøg hvad ordet *Governance* betyder oversat til dansk.
2. I jeres gruppe find kilder på internettet der definerer hvad IT-Governance er
3. Ud fra jeres research skal i blive enige i gruppen om en forklaring på hvad IT-Governance er, brug jeres egne ord og forståelse.
4. Skriv jeres forklaring i **sektion 2** i denne padlet [https://padlet.com/3ksdwqjwep/hvad-er-it-governance-c939zrgbl67q8k0t](https://padlet.com/3ksdwqjwep/hvad-er-it-governance-c939zrgbl67q8k0t)

## Links

- [Romero, S. (2011). Us and Them. In: Eliminating “Us and Them”](https://link.springer.com/chapter/10.1007/978-1-4302-3645-0_1)
- [Hvad er it-governance?](https://ing.dk/artikel/hvad-er-it-governance)
- [IT-governance på Aarhus Universitet](https://medarbejdere.au.dk/strategi/itgovernance)