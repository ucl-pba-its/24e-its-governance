---
hide:
  - footer
---

# Øvelse 20 - GDPR overblik

## Information

I denne øvelse skal i repetere lidt af det i har læst i forberedelsen.  

Formålet er at få motioneret jeres viden om GDPR og databeskyttelsesloven i passende og tilstrækkelig grad til at i har et overblik over begge. 

I bør notere jeres overvejelser, drøftelser og svar i et fælles dokument, mange af de ting i undersøger vil være direkte anvendelige til eksamen. 

**Vi samler op på klassen efter i har arbejdet med øvelsen.**

## Instruktioner

Brug links herunder når i arbejder. I jeres gruppe besvar følgende:  

1. GDPR Hvad er det?  
2. Hvad er formålet med GDPR?
3. Hvornår trådte GDPR i kraft?
4. Hvor mange kapitler indeholder GDPR?
5. Hvad er de enkelte kapitlers titel? 
6. Hvor mange artikler indeholder GDPR?
7. Er der forskel på GDPR og databeskyttelsesloven?

## Links

- [Europa-Parlamentets og Rådets forordning (EU) 2016/679 af 27. april 2016](https://eur-lex.europa.eu/legal-content/DA/TXT/PDF/?uri=CELEX:32016R0679)
- [GDPR.DK](https://gdpr.dk)
- [Databeskyttelsesloven - LBK nr 289 af 08/03/2024](https://www.retsinformation.dk/eli/lta/2024/289)