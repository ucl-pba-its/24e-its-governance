---
hide:
  - footer
---

# Øvelse 6 - Standarder i sikkerhedsarbejdet

## Information

Formålet med øvelsen er at forstå hvad et Information Security Management System (ISMS) er, hvilke aktuelle sikkerhedsstandarder der findes samt et overblik over hvordan ISO 27000 serien er opbygget med fokus på ISO27001, 27002 og 27005.

Det er en forudsætning at du har hentet ISO27001, 27002 og 27005 dokumenterne via:  
[https://sd.ds.dk/Account/LogOn?ReturnUrl=%2f](https://sd.ds.dk/Account/LogOn?ReturnUrl=%2f)

## ISMS

ISMS står for **Information Security Management System**.  
ISMS handler om at etablere en struktureret og veldokumenteret styring af informationssikkerhed i organisationen.   

Systemets vigtigeste funktion er at sikre at informationssikkerhed ikke kun er noget der tages stilling til når skaden er sket.  
Det sikrer et kontinuerligt og systematisk arbejde med informationssikkerhed. I hele organisationen og med stilligstagen til alle aktiver.

Centralt i et ISMS er et årshjul der beskriver hvornår de forskellige processer udføres.  
Eksempler på processer kan være trusselsvurdering, Identifikation af nye systemer, opdatering af risikomatrix, test af sikkerheden osv. 
Det er organisationen der beslutter hvilke processer der skal udføres og hvornår de skal udføres. 

## Standarder

Der findes flere forskellige standarder til informationssikkerhed.

### NIST CSF
Den amerikanske organisation National Institute of Standards and Technology (NIST) udgiver [NIST Cybersecurity Framework (NIST CSF)](https://nvlpubs.nist.gov/nistpubs/CSWP/NIST.CSWP.29.pdf).  

NIST Cybersecurity Framework er et sæt retningslinjer og best practices. Det blev oprindeligt udformet i 2014 som et værktøj til at hjælpe organisationer – både private og offentlige – med at forbedre deres cybersikkerhed og beskytte kritisk infrastruktur mod cybertrusler.  

Formålet med NIST Cybersecurity Framework er at give organisationer en fleksibel, risikobaseret tilgang til at identificere, vurdere og håndtere cybersikkerhedsrisici.  
Det er ikke en juridisk bindende standard, men det er bredt anerkendt og anvendt globalt som en god praksis indenfor cybersikkerhed.  

### CIS18 kontroller

CIS18 kontrollerne (tidligere kendt som CIS20) er et sæt af best practice for cybersikkerhed, udviklet af Center for Internet Security (CIS).  
De er designet til at hjælpe organisationer med at beskytte deres systemer mod de mest almindelige cybertrusler.  

De kan anvendes som en effektiv tilgang til at styrke organisationers sikkerhed, uanset deres størrelse eller sektor og de er meget mere konkrete i forhold til andre standarder.  

Du kan kigge i CIS18 kontrollernes indhold her:  
[CIS Controls Assessment Specification](https://controls-assessment-specification.readthedocs.io/en/stable/index.html)  

### ISO2700x

[ISO](https://www.iso.org) er en international standardiseringsorganisation som laver standarder for mange forskellige ting.  

ISO beskriver standarder som:  
_Standards are the distilled wisdom of people with expertise in their subject matter and who know the needs of the organizations they represent – people such as manufacturers, sellers, buyers, customers, trade associations, users or regulators._
_[ISO - What is a standard?](https://www.iso.org/standards.html)_

At følge eller være certificeret i forhold til en ISO standard er et kvalitetsmærke som virksomheder fremviser for deres kunder.  

![ISO "reklame" på bil](../images/ISO_ad_on_car.png)  

ISO 2700x serien består af en række dokumenter, der vejleder organisationer og virksomheder, som ønsker at standardisere deres tilgang til informationssikkerhed.  

Serien indeholder omkring 60 dokumenter hvor nogle er generelle og andre gælder for specialtilfælde.  

Hoveddokumenterne er ISO27001 og 27002. Tilsammen udgør de grundlaget for et ISMS.  

ISO27005 er en guide til risikostyring baseret på ISO´s generelle tilgang til ririkostyring. Det vil sige at hvis du kender tilgangen fra en anden ISO standard så vil det ligne meget af det som er beskrevet i ISO27005.  

Jeg vil ikke forklare yderligere om ISO2700x for nuværende men i stedet lade jer undersøge standarden på egen hånd (med hjælp fra min spørgsmål).  
De fleste spørgsmål kan besvares med indeholdet i ISO dokumenterne, men et par stykker kan kræve yderligere research.  

## Instruktioner

1. Undersøg hvad hhv. ISO27001, 27002 og 27005 indeholder og beskriver.
2. Undersøg hvad et ISMS er ifølge ISO27001.
3. Hvordan er foranstaltningerne i ISO27002 kategoriseret? (4 kategorier)
4. Hvad er forskellen mellem at være ISO27000 compliant og ISO27000 certificeret?
5. Skal statslige virksomheder certificeres efter ISO27000?
6. Find 2 virksomheder der er ISO27000 certificeret.
7. Lav en liste der opsummerer Figur 1 fra ISO27005 side 8 - Risikoledelsesproces for informationssikkerhed
8. Opsamling på klassen afrunder øvelsen

## Links

...