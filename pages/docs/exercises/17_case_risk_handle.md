---
hide:
  - footer
---

# Øvelse 17 - Virksomhedscase: Risikohåndtering

## Information

**Dette er en gruppeøvelse**

Formålet med risikohåndteringen er at identificere foranstaltninger der kan nedbringe risikoen for _**Sentinel Surveillance Systems**_, altså sandsynlighed og/eller konsekvens til et acceptabelt niveau.  

Foranstaltninger er mange ting og ISO/IEC 27005:2022 er et godt sted at starte. 

Husk også de 4 muligheder for at håndtere risikoen:  

1. **Acceptere risikoen:** Der er så simplet som det lyder, i vælger ikke at gøre noget. Det kan skyldes at det bliver for dyrt at håndtere risikoen, at det ikke er muligt at gøre noget (meteornedslag??). Det lyder måske ligegyldigt men faktisk er fordelen at i nu er bevidste om at risikoen eksisterer og kan addresere den senere hvis det bliver nødvendigt.
2. **Undgå risikoen:** Det gøres ved at stoppe eller ændre den aktivitet der er årsag til risikoen. For eksempel at slukke for den sårbare server eller fjerne personfølsom data fra et system.  
3. **Flytte/dele risikoen:** For eksempel ved at få nogle andre til at drifte aktiviteten eller købe en forsikring.
4. **Forøge/minimere risikoen:** ved at implementere foranstaltninger der mindsker sandsynlighed og/eller konsekvens. Det kan være kryptering osv. ISO/IEC 27002:2022 har en række af sikkerhedsforanstaltninger der kan anvendes som inspiration.  
Det lyder måske ikke så smart at forøge en risiko, men nogle gange gøres det ud fra et forretningshensyn. Her accepteres en større risiko fordi gevinsten ved satset er for stort til at undlade risikoen. Et eksempel er at indtræde på et nyt marked og de risici der følger med det.

## Instruktioner

For hver risici der er enten gule eller røde:  

1. Hvordan skal risikoen håndteres? Undgå, flytte/dele eller forøge/minimere.
2. Find en eller flere foranstaltninger i ISO/IEC 27002:2022 som kan nedbringe risikoen. I jeres gruppe skal i sammen begrunde hvordan foranstaltningen nedbringer risikoen.
3. Beskriv hvilket niveau risikoen nedbringes til. Hvis det ikke er muligt at nedbringe risikoen til et acceptabelt niveau så snak om hvordan i vil kommunikere risikoen til produktejer og eller direktionen (så de forstår det). 
4. Del jeres output fra øvelse 15, 16 og 17 med mig i afleveringsopgaven på itslearning - den er på dagens plan og hedder **"Afleveringsopgave: Risikostyring - Sentinel Surveillance Systems"**   

## Links

- ...