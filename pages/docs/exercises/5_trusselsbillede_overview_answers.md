---
hide:
  - footer
---

# Øvelse 5 - Trusselsbillede, et overblik (SVAR)

## Information

Dette er undervisers svar på [Øvelse 5 - Trusselsbillede, et overblik](../exercises/5_trusselsbillede_overview.md).

Svarene er givet som bedste bud, det vil sige at det ikke nødvendigvis er et facit men snarere undervisers opfattelse af svar.  



## Instruktioner

1. ranger efter cfcs trusselsniveau for hver kategori  
    _Rangéret ifølge [Cybertruslen mod Danmark 2023](https://www.cfcs.dk/da/cybertruslen/trusselsvurderinger/arkiv/cybertruslen-mod-danmark-2023/)_
    - MEGET HØJ (cyberkriminalitet): Cyberkriminalitet (Er der penge i det?)
    - MEGET HØJ (cyberspionage): Cyberspionage (mod lande eller virksomheder?)
    - MEGET HØJ (cyberspionage): Overvågning (hvem kan dog finde på at overvåge andre?)
    - HØJ (cyberaktivisme): Hacktivisme (er hackere onde??)
    - HØJ (cyberaktivisme): Politisk påvirkning (Valg i USA?)
    - LAV (destruktive cyberangreb): Cyberkrig (Ukraine/Rusland?)
    - LAV (destruktive cyberangreb) Cyberterrorisme (elbiler? kritisk infrastruktur?)
2. Find et eksempel fra virkeligheden der passer på hver kategori, start med dem der har det højeste trusselsniveau.
    - Cyberkriminalitet (Målet er penge)
        - [Hackerangreb koster Mærsk milliardbeløb](https://www.dr.dk/nyheder/penge/hackerangreb-koster-maersk-milliardbeloeb)
        - [it-hotellet](https://www.it-hotellet.dk/)
        - [Crime-As-A-Service](https://www.europol.europa.eu/cms/sites/default/files/documents/Spotlight%20Report%20-%20Cyber-attacks%20the%20apex%20of%20crime-as-a-service.pdf)
    - Cyberspionage (mod lande og virksomheder)
        - [Edward Snowden: Leaks that exposed US spy programme](https://www.bbc.com/news/world-us-canada-23123964)
        - [Marriott data breach FAQ: How did it happen and what was the impact?](https://www.csoonline.com/article/567795/marriott-data-breach-faq-how-did-it-happen-and-what-was-the-impact.html)
        - [The U.S.-China Cyber Espionage Deal One Year Later](https://www.cfr.org/blog/us-china-cyber-espionage-deal-one-year-later)       
    - Overvågning
        - [Vi bør følge AI Acts regler for politiet, ellers mister vi konkurrenceevne](https://radar.dk/holdning/dansk-producent-af-ansigtsgenkendelse-vi-boer-foelge-ai-acts-regler-politiet-ellers-mister-vi)
        - [Ekstrem overvågning i Kina - alle får point for deres opførsel](https://nyheder.tv2.dk/udland/2018-04-15-ekstrem-overvaagning-i-kina-alle-faar-point-for-deres-opfoersel)
        - [Danmark lod firma sælge overvågningsteknologi til mellemøstligt regime](https://www.dr.dk/nyheder/indland/trods-britisk-advarsel-danmark-lod-firma-saelge-overvaagningsteknologi-til)
    - Hacktivisme
        - [wikileaks](https://wikileaks.org/)
        - [Anonymous 'declares war' on Turkey](https://www.independent.co.uk/tech/anonymous-declares-war-on-turkey-opsis-russia-cyberattack-erdogan-a6784026.html)
    - Politisk påvirkning
        - [Anklage: Rusland hyrede amerikanske influencere til at påvirke det kommende valg](https://www.dr.dk/nyheder/udland/valg-i-usa/anklage-rusland-hyrede-amerikanske-influencere-til-paavirke-det-kommende)
        - [Putins propagandamaskine: Sådan ser krigen ud på russisk stats-tv](https://www.dr.dk/nyheder/udland/hvilke-loegnhistorier-fortaeller-russiske-medier)
        - [Forsker: Rusland har en klar interesse i at påvirke valg i Tyskland](https://www.dr.dk/nyheder/udland/forsker-rusland-har-en-klar-interesse-i-paavirke-valg-i-tyskland)
    - Cyberkrig (Ukraine/Rusland)
        - [Danger Close: Fancy Bear Tracking of Ukrainian Field Artillery Units](https://www.crowdstrike.com/blog/danger-close-fancy-bear-tracking-ukrainian-field-artillery-units/)
        - [Falsk Tinder-babe lokkede soldater til at ignorere ordrer og forlade deres poster ](https://www.tjekdet.dk/indsigt/falsk-tinder-babe-lokkede-soldater-til-ignorere-ordrer-og-forlade-deres-poster)
    - Cyberterrorisme 
        - [Car Hackers Are Out for Blood](https://www.theatlantic.com/technology/archive/2023/09/electric-car-hacking-digital-features-cyberattacks/675284/)
        - [Rapport: Angrebet mod dansk, kritisk infrastruktur ](https://sektorcert.dk/wp-content/uploads/2023/11/SektorCERT-Angrebet-mod-dansk-kritisk-infrastruktur-TLP-CLEAR.pdf)

## Links

...