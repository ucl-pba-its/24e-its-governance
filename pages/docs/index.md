---
hide:
  - footer
  - toc
---

# IT-Governance efterår 2024

På dette website finder du ugeplaner, Øvelser, dokumenter og links til brug i faget _IT-Governance_

På UCL's Learning Management Sysytem (LMS, pt. ITSLearning) bør du kunne danne dig samme overblik som her, dog kræver denne side ikke login, samt der kan søges på siden.

## Fagets indhold

Faget omhandler Etik og principper for it-sikkerhed samt introduktion til principper og begreber inden for it-sikkerhed.  
Den studerende får en forståelse for grundlæggende principper og antagelser i it-sikkerhedsarbejde, og introduceres til etiske og politiske aspekter af samme.

## Studieordning

Studieordningen er i 2 dele, en national del og en institutionel del.

Begge dele kan findes i [studiedokumenter på ucl.dk](https://www.ucl.dk/studiedokumenter/it-sikkerhed)


## Læringsmål fra studieordningen

### Viden

Den studerende har viden om og forståelse for: 

- Videregående it-governance
- Principper indenfor it-sikkerhed
- Risikoanalyse
- Standarder og organisationer i sikkerhedsarbejdet
- Trusler og trusselsbilledet
- Operationelle overvejelser for it-sikkerhed
- Sikkerhedspolitikker og -procedurer (Business Continuity and Disaster Recovery).

### Færdigheder

Den studerende kan:

- Foretage risikovurdering af mindre systemer/virksomheder, herunder datasikkerhed.
- Vurdere hvilke sikkerhedsprincipper, der skal anvendes i forhold til en given kontekst.

### Kompetencer

Den studerende kan:

- Håndtere analyser om, hvilke sikkerhedstrusler der aktuelt skal behandles i et konkret it-system.
