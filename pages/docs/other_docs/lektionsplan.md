---
title: '24E PBa IT-Sikkerhed'
subtitle: 'Fagplan IT-Governance'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
email: 'nisi@ucl.dk'
left-header: \today
right-header: Fagplan for IT-Governance
skip-toc: false
semester: 24E
hide:
  - footer
---

# Introduktion

Fagområdet indeholder ...

Faget er på XX ECTS point.

# Læringsmål

**Viden**

Den studerende har ...

- ..
- ..


**Færdigheder**

Den studerende kan ...

- ..
- ..

**Kompetencer**

Den studerende kan ...

- ..
- ..

# Lektionsplan

Se ITSLearning

# Studieaktivitets modellen

![study activity model](Studieaktivitetsmodellen.png)

## Andet

Intet på nuværende tidspunkt
