---
title: '24E ITS IT-Governance eksamen emner'
subtitle: '24E ITS IT-Governance eksamen emner'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2024-10-04
email: 'nisi@ucl.dk'
right-header: IT-Governance eksamen emner
hide:
  - footer
---

# 24E ITS IT-governance og Videregående IT-governance eksamenopgave

## Dokumentets indhold

Dette dokument indeholder praktiske informationer samt spørgsmål til eksamen i faget: 24E IT-Governance og Videregående IT-governance

## Eksamens beskrivelse

Eksamen er beskrevet i den institutionelle del af studieordningen (_afsnit 3.2.5_ og _afsnit 3.2.6_)

Den individuelle, mundtlige eksamen varer 25 minutter inkl. votering.  

- 10 minutter til den studerendes præsentation
- 10 minutter til spørgsmål fra eksaminator og censor
- 5 minutter til votering

Tidsplan for eksamen kan findes på wiseflow.  

Eksamen er med intern censur.  

### **Fra studieordningen:**  

Prøven er en individuel, mundtlig prøve med udgangspunkt i et spørgsmål, som den studerende trækker til eksamen.  

Alle spørgsmål, der kan trækkes til eksamen, er udleveret til de studerende senest 14 dage før eksamen, så de studerende har mulighed for at forberede sig.   
Hvad angår spørgsmålene, vil der blive lagt vægt på, at den studerende kan inddrage eksempler fra
projektarbejdet og praktiske øvelser fra det forgangne semester.  

Der er ingen forberedelse på selve dagen.  

Prøven dækker fagene IT-governance og Videregående IT-governance og altså samlet set 10 ECTS-point.

## Emner

Nedenstående emner skal opfattes som eksamensspørgsmål. De er udformet bredt i overskrifter der beskriver emnet og hjælpespørgsmål der kan understøtte den studerende i inddragelse af opnået viden, færdigheder og kompetencer fra undervisningen.  
Den studerende bør forberede en præsentation for hvert af de 5 emner som forberedelse til eksamen.  
Det er ikke tilladt at gengive fagets undervisningsmateriale i præsentationen.  

1. **Governance og Compliance:** Hvad er det og hvad er vigtigt i arbejdet med Governance og
Compliance? (Lovgivning, standarder, best practices)
2. **Trusselsbillede:** hvad er det i en virksomhedskontekst. (trusslesaktører, kategorier, trusselsniveau, eksempler fra virkeligheden) 
3. **Risikostyring:** hvad er det i en sikkerhedskontekst? Hvilke virksomheder bør anvende risikostyring, hvorfor og hvordan? (lovgivning, standarder, konkurrencedygtighed, risikostyringsprocessen ifølge ISO/IEC 27005)
4. **Beredskabsstyring:** Hvordan etableres og vedligeholdes et informationssikkerhedsberedskab (de fire faser, hvordan kan beredskabsøvelser udføres og hvordan hjælper de virksomheden?) 
5. **GDPR:** Hvad er det? Hvilke rettigheder har de registrerede i persondataforordningen?
(Dataetik, ansvarlighed, tilstrækkelighed, passende, indsigt)
6. **NIS2**: Hvad kommer NIS2 til at betyder for virksomhederne? Hvem er inkluderet? Findes der værktøjer, der kan hjælpe virksomhederne?
7. **Internationale Cybersikkerhedsstandarder ISO 27001/ISO27002:** Hvad kan standarderne bruges til? Giv eksempler på anvendelse af standarderne?
8. **Supply Chain Management:** Hvorfor er det vigtigt? Kan vi forhindre angreb i virksomheders Supply Chain?
9. **Hvad er awareness?** Hvad skal der, efter din mening, være til stede for at awareness virker? Hvad er smishing, vishing og phishing? Hvilke tiltag kan en virksomhed iværksætte for at sikre sig bedst muligt? 
10. **GDPR:** Hvem er databehandler, og hvem er dataansvarlig? Hvad siger straffeloven om databehandler og dataanvarlig? Er der specielle persongrupper GDPR nævner? Hvad skal man være opmærksom på, når man overfører data (3. lande)


## Eksamens datoer

Se semesterbeskrivelsen - [https://esdhweb.ucl.dk/D24-2601256.pdf](https://esdhweb.ucl.dk/D24-2601256.pdf)
